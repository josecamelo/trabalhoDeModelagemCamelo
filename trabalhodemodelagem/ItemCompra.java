package trabalhodemodelagem;

public class ItemCompra {

	private int quantidade;
    private float valor;
    private Produto  produto;
    
    public ItemCompra(int quantidade,Produto produto){
        this.quantidade=quantidade;
        this.valor +=produto.getpreco()*quantidade;
        this.produto=produto;
    }
    public ItemCompra(){
    
}
    public int getQuantidade(){   
        return quantidade;
    }
    public void setQuantidade(int quantidade){
        this.quantidade=quantidade;
        
    }
    public float getvalor(){
        return valor;
    }
    public void setvalor(int valor){
        this.valor=valor;
    }
    public Produto getproduto(){
       return produto; 
    }
     public void adicionarProduto(Produto produto) {
        this.produto = produto;
        
     }
}


